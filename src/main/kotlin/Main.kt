package org.example
const val DOMAIN="student.kdg.be"
const val GOOGLE_DEV_PREFIX="g.dev/"
const val GOOGLE_DEV_SUFFIX="-kdg"

 fun toEmail(firstName:String,lastName:String):String{
    return normalizeName("$firstName.$lastName")+ "@$DOMAIN"
}


fun normalizeName(name:String) =  name.replace(" ","").lowercase()

fun printConverted(firstName:String,lastName:String,converter: (String,String)->String){
    println(converter(firstName,lastName))
}


fun main() {
    println(toEmail("Jan","de Rijke"))
    printConverted("Reginald","Moreels", ::toEmail)
    // one line lambda
    printConverted("Evert","Vennema",
        {firstName,lastName -> "$GOOGLE_DEV_PREFIX${normalizeName(firstName+lastName)}$GOOGLE_DEV_SUFFIX"}
    )
    // multiline lambda
    printConverted("Evert","Vennema",
        {firstName,lastName ->
            val name = normalizeName(firstName + lastName)
            "$GOOGLE_DEV_PREFIX$name$GOOGLE_DEV_SUFFIX"
        }
    )
    // lambda out of parameters
    printConverted("Evert","Vennema" ) { firstName, lastName ->
        val name = normalizeName(firstName + lastName)
        "$GOOGLE_DEV_PREFIX$name$GOOGLE_DEV_SUFFIX"
    }
}